// Diseño: counter
// Descripción: Contador ascendente módulo 16. Banco de pruebas.
// 2019-02-13 Jorge Juan-Chico <jjchico@dte.us.es>

// Escala de tiempo y resolución de la simulación
`timescale 1ns / 1ps

//
// Banco de pruebas
//
module test();

    // En un banco de pruebas todas las señales son internas
    // entradas al contador
    reg clk, clear, up;
    // salidas del contador
    wire [3:0] q;
    wire cy;

    // Instancia del circuito bajo test
    counter uut (.clk(clk), .up(up), .clear(clear), .q(q), .cy(cy));

    // Procedimiento de generación de la señal de reloj: cada 10ns
    // conmutamos el valor de la señal de reloj (periodo = 20ns)
    always
        #10 clk = ~clk;

    // Procedimiento de inicialización y control de la simulación
    initial begin
        // Valor inicial de las señales
        clk = 0;
        clear = 0;
        up = 0;

        // Generación de formas de onda
        $dumpfile("counter_tb.vcd");    // Archivo de datos
        $dumpvars(0, test);             // Señales a guardar (todas)

        // Control de la simulación
        /* Damos valores a las entradas del contador de forma que usemos
         * toda su funcionalidad. Cambiamos las señales en los flancos de
         * bajada del reloj (flanco no activo): @(negedge clk) ...
         */
        @(negedge clk) clear = 1;    // activamos clear
        @(negedge clk) clear = 0;    // resultado esperado: q = 0;
        @(negedge clk) up = 1;       // activamos señal de cuenta. Dejamos pasar
        repeat(20) @(negedge clk);   // 20 ciclos (habrá desbordamiento)
        up = 0;                      // paramos la cuenta y esperamos 5 ciclos
        repeat(5) @(negedge clk);
        clear = 1;                   // hacemos un nuevo clear y esperamos
        repeat(2) @(negedge clk);    // dos ciclos
        clear = 0;

        // esperamos 50ns y terminamos la simulación
        #50 $finish;
    end

endmodule // test

/*
   Puede compilar y simular este diseño con Icarus Verilog ejecutando en
   un terminal:

   $ iverilog counter.v counter_tb.v
   $ vvp a.out

   Puede ver los resultados de simulación con el visor de ondas Gtkwave
   ejecutando en un terminal:

   $ gtkwave counter_tb.vcd &
 */
