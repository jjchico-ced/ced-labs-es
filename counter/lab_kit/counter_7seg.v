// Diseño: counter_7seg
// Descripción: Contador ascendente módulo 16. Salida 7 segmentos
// 2019-02-13 Jorge Juan-Chico <jjchico@dte.us.es>

// Escala de tiempo y resolución de la simulación
`timescale 1ns / 1ps

module counter_7seg(
    input wire clk,         // señal de reloj
    input wire clear,       // puesta a cero síncrona
    input wire up,          // cuenta ascendente
    output wire [0:6] seg,  // salida de cuenta (7 segmentos)
    output wire cy,         // fin de cuenta (carry)
    output wire [3:0] an,   // cifra activa
    output wire dp          // punto decimal
    );

    wire [3:0] q;    // conexión del contador con convertidor 7 segmentos

    /* introduce aquí una instancia del contador módulo 16 */


    /* introduce aquí una instancia del convertidor 7 segmentos, conectado a
     * la salida del contador módulo 16 */


    /* genera aquí los valores constantes para an y dp (usa 'assign') */


endmodule // counter_7seg
