Prácticas de laboratorio de Circuitos Electrónicos Digitales
============================================================

Diseño de prácticas de laboratorio para la asignatura "Circuitos Electrónicos
Digitales" de las titulaciones de Ingeniería Informática de la ETSI Informática
de Sevilla.

El material en este repositorio es un espacio de trabajo del autor y no tiene
por qué coincidir con prácticas impartidas oficialmente en las asignaturas.

Listado de prácticas
--------------------

* [Counter](counter/): Contador con salida en código 7 segmentos (Verilog).

* [Electronic Key](electronic_key/): Cerradura electrónica (Verilog).

Autor
-----

Jorge Juan-Chico <jjchico@dte.us.es>

Licencia
--------

CC BY-SA 4.0 [Creative Commons Atribution-ShareAlike 4.0 International] (https://creativecommons.org/licenses/by-sa/4.0/)
